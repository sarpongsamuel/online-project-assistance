
<?php include '../../core/init.php'; ?>
<?php
if($getFromU->loggedIn() === false){
  $getFromU->userRedirect();
}
//fetch user data
$user_id =$_SESSION['user_id'];
$user = $getFromU->userData($user_id);

// if completebutton is clicked
if(isset($_GET['complete'])){
  $task_id =$_GET['complete'];
  $getFromU->updateTask($task_id);
 // header('Location: task.php');

}
 ?>

<!DOCTYPE html>
<html lang="en">
<?php include 'includes/head.php'; ?>
<!-- <script src="js/search.js"></script> -->
<body>
  <section id="container">
    <?php include 'includes/header.php'; ?>
    <!--sidebar start-->
    <?php include 'includes/aside.php'; ?>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading wht-bg">
                  <h4 class="gen-case">
                      All Completed Task
                      <form action="" class="pull-right mail-src-position" method="post">
                        <div class="input-append">
                          <input type="text" class="form-control search" placeholder="Search completed task">
                        </div>
                      </form>
                    </h4>
                </header>
                <div class="panel-body minimal">
                  <div class="mail-option">
                    <div class="chk-all">
                      <div class="pull-left mail-checkbox">
                        <input type="checkbox" class="">
                      </div>
                      <div class="btn-group">
                        <a data-toggle="dropdown" href="#" class="btn mini all">
                          All
                          </a>
                      </div>
                    </div>
                    <div class="btn-group">
                      <a data-original-title="Refresh" data-placement="top" data-toggle="dropdown" href="" class="btn mini tooltips">
                        <i class=" fa fa-refresh"></i>
                        </a>
                    </div>
                    <!-- <ul class="unstyled inbox-pagination">
                      <li><span>1-50 of 99</span></li>
                      <li>
                        <a class="np-btn" href="#"><i class="fa fa-angle-left  pagination-left"></i></a>
                      </li>
                      <li>
                        <a class="np-btn" href="#"><i class="fa fa-angle-right pagination-right"></i></a>
                      </li>
                    </ul> -->
                  </div>
                  <div class="table-inbox-wrap ">
                    <table class="table table-inbox table-hover">
                      <thead>
                        <tr>
                          <th>option</th>
                          <th>Starred</th>
                          <th>Company Name</th>
                          <th>Project Name</th>
                          <th>Project Task</th>
                          <th>Created At</th>
                          <th>Due Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <div class="search-results">

                        </div>
                        <?php $getFromU->taskCompleted($user->user_id); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
          </div>
        </div>
      </section>
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include 'includes/footer.php'; ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <?php include 'includes/javascript.php'; ?>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
</body>
</html>

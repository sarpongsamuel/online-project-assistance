
<?php include '../../core/init.php'; ?>
<?php
if($getFromU->loggedIn() === false){
  $getFromU->userRedirect();
}
//fetch user data
$user_id =$_SESSION['user_id'];
$user = $getFromU->userData($user_id);

if(isset($_POST['submit'])){
  $fullname =$_POST['fullname'];
  $password =$_POST['password'];
  $email =$_POST['email'];

  $email=$getFromU->checkInput($email);
  $password=$getFromU->checkInput($password);
  $fullname=$getFromU->checkInput($fullname);

  if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      $error ="Invalid email format";
  }else if($getFromU->checkEmail($email,$user->user_id)===true){
    $error="Email Already Taken By Another User";
  }else{
    $getFromU->update('users', $user_id, array('password'=>md5($password), 'fullname'=>$fullname, 'email'=>$email));
    header('Location: index.php');
  }

}
 ?>

<!DOCTYPE html>
<html lang="en">
<?php include 'includes/head.php'; ?>
<style media="screen">
  #main-content{
    background-color: white;
  }
  /* .alert-danger{
    background-color: red;
    color: black;
    padding: 10px;
  } */
</style>
<body>
  <section id="container">
    <?php include 'includes/header.php'; ?>
    <!--sidebar start-->
    <?php include 'includes/aside.php'; ?>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i>User Account</h3>
        <div class="row mt">
            <div class="container ">
              <div class="col-lg-12 ">
              <h1>User Details</h1><hr>
              <?php if(isset($error)){
                echo'<div class="alert alert-danger p-3 mb-2 role="alert">'.$error.'</div>';
              } ?>
              <form class="" action="account.php" method="post">
                <div class="form-group">
                  <label for="">Fullname</label>
                  <input type="text" class="form-control" name="fullname" value="<?=$user->fullname; ?>" required>
                </div>
                <div class="form-group">
                  <label for="">Email</label>
                  <input type="email" class="form-control" name="email" value="<?=$user->email; ?>" required>
                </div>
                <div class="form-group">
                  <label for="">Enter New Password</label>
                  <input type="password" class="form-control" name="password" value="<?=md5($user->fullname); ?>" required>
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-compose form-control" name="submit" value="update Details">
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include 'includes/footer.php'; ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <?php include 'includes/javascript.php'; ?>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
</body>
</html>

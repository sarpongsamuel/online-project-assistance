<?php include '../../core/init.php'; ?>
<?php
if($getFromU->loggedIn() === false){
  $getFromU->userRedirect();
}
//fetch user data
$user_id =$_SESSION['user_id'];
$user = $getFromU->userData($user_id);
 ?>

<!DOCTYPE html>
<html lang="en">
<?php include 'includes/head.php'; ?>
<body>
  <section id="container">
    <?php include 'includes/header.php'; ?>
    <!--sidebar start-->
    <?php include 'includes/aside.php'; ?>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="col-lg-12">
            <div class="row content-panel">
              <!-- <div class="col-md-4 profile-text mt mb centered">
                <div class="right-divider hidden-sm hidden-xs">

                </div>
              </div> -->
              <!-- /col-md-4 -->
              <div class="col-md-6 col-md-offset-2 profile-text">
                <h3> <?=$user->fullname; ?> </h3>
                <h6><?=$user->work; ?></h6>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.</p>
                <br>
                <p><button class="btn btn-theme"><i class="fa fa-envelope"></i> Send Message</button></p>
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 centered">
                <div class="profile-pic">
                  <p><img src="img/friends/fr-02.jpg" class="img-circle"></p>
                </div>
              </div>
              <!-- /col-md-4 -->
            </div>
            <!-- /row -->
          </div>
        </div>
        <div class="col-lg-12 mt">
          <div class="row content-panel">
            <div class="panel-heading">
              <ul class="nav nav-tabs nav-justified">
                <li class="active">
                  <!-- <a data-toggle="tab" href="#overview">Overview</a> -->
                </li>
              </ul>
            </div>
            <!-- /panel-heading -->
            <div class="panel-body">
              <div class="tab-content">
                <div id="overview" class="tab-pane active">
                  <div class="row">
                    <div class="col-md-6">

                      <div class="detailed mt">
                        <h4>Recent Activity</h4>
                        <?php $getFromU->myTask($user->user_id); ?>
                        <!-- /recent-activity -->
                      </div>
                      <!-- /detailed -->
                    </div>
                    <!-- /col-md-6 -->
                    <div class="col-md-6 detailed"><br>
                      <h4>User Stats</h4>
                      <div class="row centered mt mb">
                        <div class="col-sm-4">
                          <h1><i class="fa fa-money"></i></h1>
                          <h3> <?php echo $getFromU->myTaskCount($user->user_id); ?> </h3>
                          <h6>TASK ASSIGNED</h6>
                        </div>
                        <div class="col-sm-4">
                          <h1><i class="fa fa-trophy"></i></h1>
                          <h3><?php echo $getFromU->myTaskCompleted($user->user_id); ?></h3>
                          <h6>COMPLETED TASK</h6>
                        </div>
                        <div class="col-sm-4">
                          <h1><i class="fa fa-shopping-cart"></i></h1>
                          <h3><?php echo $getFromU->myTaskUnfinished($user->user_id); ?></h3>
                          <h6>PENDING TASK</h6>
                        </div>
                      </div>
                      <br>
                      <h4>User Progress </h4>
                      <div class="row centered">
                        <div class="col-md-8 col-md-offset-2">
                          <h5>TASK COMPLETED</h5>
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $getFromU->myTaskCompleted($user->user_id); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $getFromU->myTaskCompleted($user->user_id); ?>%">
                              <span class="sr-only"><?php echo $getFromU->myTaskCompleted($user->user_id); ?> Complete (success)</span>
                            </div>
                          </div>

                          <h5>TASK ASSIGNED <?php echo $getFromU->myTaskCount($user->user_id); ?>%</h5>
                          <div class="progress">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo $getFromU->myTaskCount($user->user_id); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $getFromU->myTaskCount($user->user_id); ?>%">
                              <span class="sr-only"></span>
                            </div>
                          </div>
                        </div>
                        <!-- /col-md-8 -->
                      </div>
                      <!-- /row -->
                    </div>
                    <!-- /col-md-6 -->
                  </div>
                  <!-- /OVERVIEW -->
                </div>
                <!-- /tab-pane -->
              </div>
              <!-- /tab-content -->
            </div>
            <!-- /panel-body -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
      </div>
      </section>
    </section>
    <?php include 'includes/footer.php'; ?>
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <?php include 'includes/javascript.php'; ?>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
</body>
</html>

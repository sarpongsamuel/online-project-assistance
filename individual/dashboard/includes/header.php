<header class="header black-bg">
  <div class="sidebar-toggle-box">
    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
  </div>
  <!--logo start-->
  <a href="index.php" class="logo"><b>O<span>PA</span></b></a>
  <!--logo end-->
  <div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <ul class="nav top-menu">
      <!-- settings start -->
      <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
          <i class="fa fa-tasks"></i>
          <span class="badge bg-theme"><?php echo  $getFromU->myTaskUnfinished($user_id); ?></span>
          </a>
        <ul class="dropdown-menu extended tasks-bar">
          <div class="notify-arrow notify-arrow-green"></div>
          <li>
            <p class="green">You have <?php echo  $getFromU->myTaskUnfinished($user_id); ?> pending tasks</p>
          </li>
   <?php $getFromU->pendingTask($user_id); ?>
        </ul>
      </li>
      <!-- settings end -->
      <!-- inbox dropdown start-->

      <!-- inbox dropdown end -->
      <!-- notification dropdown start-->
      <!-- <li id="header_notification_bar" class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
          <i class="fa fa-bell-o"></i>
          <span class="badge bg-warning">7</span>
          </a>
        <ul class="dropdown-menu extended notification">
          <div class="notify-arrow notify-arrow-yellow"></div>
          <li>
            <p class="yellow">You have 7 new notifications</p>
          </li>

          <li>
            <a href="index.html#">See all notifications</a>
          </li>
        </ul>
      </li>
      <!- notification dropdown end -->
    <!-- </ul>  -->
    <!--  notification end -->
  </div>
  <div class="top-menu">
    <ul class="nav pull-right top-menu">
      <li><a class="logout" href="../logout.php">Logout</a></li>
    </ul>
  </div>
</header>

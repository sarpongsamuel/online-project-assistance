<aside>
  <div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <p class="centered"><a href="account.php"><img src="img/ui-sherman.jpg" class="img-circle" width="80"></a></p>
      <h5 class="centered"> <?php echo $user->fullname; ?> </h5>
      <li class="mt">
        <a href="index.php">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
          </a>
      </li>
      <li class="sub-menu">
        <a href="javascript:;">
          <i class="fa fa-desktop"></i>
          <span>MyCompany</span>
          </a>
        <ul class="sub">
          <?php $getFromU->userCompany($user->user_id) ?>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="task.php">
          <i class="fa fa-tasks"></i>
          <span>Task Completed</span>
          </a>
      </li>
      <li class="sub-menu">
        <a href="javascript:;">
          <i class=" fa fa-cogs"></i>
          <span>Setting</span>
          </a>
        <ul class="sub">
          <li><a href="account.php">Account Settings</a></li>
        </ul>
      </li>
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>


<?php include '../../core/init.php'; ?>
<?php
if($getFromU->loggedIn() === false){
  $getFromU->userRedirect();
}
//fetch user data
$user_id =$_SESSION['user_id'];
$user = $getFromU->userData($user_id);

if(isset($_GET['company_id'])){
  $company_id= $_GET['company_id'];
  $company =$getFromC->companyData($company_id);
}
 ?>
   <?php
   if(isset($_POST['submit'])){
    $message =$_POST['message'];
    $company_id=$_POST['company_id'];
    $message =$getFromU->checkInput($message);

    //fetch  company data
    $company =$getFromC->companyData($company_id);
    $company_name =$company->company_name;
    $sender_id =$user->user_id;
    $sender_name =$user->fullname;
    $date = date('Y-m-d');
    $getFromU->create('chats', array('sender_id'=>$sender_id, 'company_id'=>$company_id,'sender_name'=>$sender_name, 'message'=>$message, 'created_at'=>$date));
    header('Location: index.php');
   }
    ?>

<!DOCTYPE html>
<html lang="en">
<?php include 'includes/head.php'; ?>
<style media="screen">
body{
  color:black;
}
.box{
  padding:40px 0px;
}

.box-part{
  background:#FFF;
  border-radius:0;
  padding:40px 10px;
  border: 2px solid #4ECDC4;
  margin:30px 0px;
}
.title h4{
  color: #4ECDC4;
}

.fa-3x{
  background-color: #4ECDC4;
}
.text{
  margin:20px 0px;
  color: black;
}
/* Style tab links */
.tablink {
  background-color: #555;
  color: white;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  font-size: 17px;
  width: 25%;
}

.tablink:hover {
  background-color: #777;
}

/* Style the tab content (and add height:100% for full page content) */
.tabcontent {
  color: white;
  display: none;
  padding: 100px 20px;
  height: 100%;
}
.second-part{
  color:black;
}

.third-part{
  color: black;
}
#Home {background-color: white;}
#News {background-color: white;}

</style>
<body>
  <section id="container">
    <?php include 'includes/header.php'; ?>
    <!--sidebar start-->
    <?php include 'includes/aside.php'; ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="container">
          <div class="col-lg-12">
            <button class="tablink" onclick="openPage('Home', this, 'grey')" id="defaultOpen">Task</button>
            <button class="tablink" onclick="openPage('News', this, 'grey')" >Lobby</button>

            <div id="Home" class="tabcontent">
           <div class="row">
             <?php $getFromU->getUserTask($user->user_id,$company->company_id); ?>
           </div>
            </div>

            <div id="News" class="tabcontent">
              <div class="chat-room mt">
                <aside class="mid-side">
                  <div class="chat-room-head">
                    <h3>Chat Room: Support</h3>
                    <form action="#" class="pull-right position" >
                      <input type="text" placeholder="Search" class="form-control search-btn ">
                    </form>
                  </div>
                  <?php $getFromC->messages($company->company_id); ?>
                  <div class="group-rom last-group">
                    <div class="first-part odd"></div>
                    <div class="second-part"></div>
                    <div class="third-part"></div>
                  </div>
                  <footer>
                    <form class="" action="company.php" method="post">
                    <div class="chat-txt">
                        <input type="hidden" class="form-control" name="company_id" value="<?=$company->company_id ?>">
                        <input type="text" class="form-control" name="message" placeholder="message">
                    </div>
                    <div class="btn-group hidden-sm hidden-xs">
                      <button type="button" class="btn btn-white"><i class="fa fa-meh-o"></i></button>
                      <button type="button" class="btn btn-white"><i class=" fa fa-paperclip"></i></button>
                    </div>
                    <!-- <button class="btn btn-theme">Send</button> -->
                    <input type="submit" class="btn btn-theme" name="submit" value="send">
                    </form>
                  </footer>
                </aside>
              </div>
            </div>

          </div>
          </div>
        </div>
      </section>
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include 'includes/footer.php'; ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <?php include 'includes/javascript.php'; ?>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
<script type="text/javascript">
  function openPage(pageName, elmnt, color) {
// Hide all elements with class="tabcontent" by default */
var i, tabcontent, tablinks;
tabcontent = document.getElementsByClassName("tabcontent");
for (i = 0; i < tabcontent.length; i++) {
  tabcontent[i].style.display = "none";
}
// Remove the background color of all tablinks/buttons
tablinks = document.getElementsByClassName("tablink");
for (i = 0; i < tablinks.length; i++) {
  tablinks[i].style.backgroundColor = "";
}
// Show the specific tab content
document.getElementById(pageName).style.display = "block";
// Add the specific color to the button used to open the tab content
elmnt.style.backgroundColor = color;
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
  <!--script for this page-->
</body>
</html>

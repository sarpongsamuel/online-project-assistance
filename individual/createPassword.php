<?php include '../core/init.php'; ?>

<!-- fetch user details using step id -->
<?php
if(isset($_GET['user'])){
  $user_id =$_GET['user'];
  //call userdata method;
  $user = $getFromU->userData($user_id);
}

//post to login
if(isset($_POST['submit'])){
 $password = $_POST['password'];
 $password= $getFromU->checkInput($password);
 $user_id = $user->user_id;
 $getFromU->update('users', $user_id, array('password'=>md5($password)));
 //login user
 $getFromU->login($user->email,$password);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Online Project Assistance</title>
	<link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="css/slick.css" />
	<link type="text/css" rel="stylesheet" href="css/slick-theme.css" />
	<link type="text/css" rel="stylesheet" href="css/nouislider.min.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link type="text/css" rel="stylesheet" href="css/style.css" />
</head>
<style media="screen">

	.container{
		margin-top: 5%;
	}
	.headtext{
		font-size: 30px;
    color:white;
		font-family: serif;
    text-align: center;
	}
  .navbar{
    background: transparent;
  }
  .subtext{

    color: white;
    font-size: 15px;
  }

  h1 strong{
    font-size: 35px;
    text-align: center;
  }
  .hey{
    text-align: center;
  }
  .top{
    background-color: #4ECDC4;
    padding: 30px

  }
  .name{
    font-size: 50px;
    text-align: center;
  }
  .btn-save{
    background-color: #4ECDC4;
    color: black;
  }
  p{
    text-align: center;
  }
  input[type='password']{
    height: 50px;
    border-radius: 0;
  }

  input[type='submit']{
    height: 50px;
    border-radius: 0;
  }
</style>
<body>
<div class="container-padded">
  <div class="row">
    <div class="col-md-12 top">
      <h1 class="headtext">Online Project Assistance</h1>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-2">

    </div>
    <div class="col-md-8">
      <br>
      <h1 class="hey"> <strong>HI</strong> </h1>
      <h2 class="name"> <?php echo $user->fullname ?> </h2>
      <p> <em>Set Your New Password <br>Dont worry You can Always change it later </em> </p> <br>
      <form class="" action="" method="post">
        <input type="password" name="password" value=""placeholder="Enter New Password" class="form-control"><br>
        <input type="submit" name="submit" value="Save Password" class="form-control btn btn-save btn-sm">
      </form>
    </div>
  </div>
</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>

<?php include 'core/init.php'; ?>
<?php
if(isset($_POST['submit'])){
	$company_name =$_POST['company_name'];
	$fullname =$_POST['fullname'];
	$email =$_POST['email'];
	$password =$_POST['password'];

	//sanitize
	$fullname=$getFromC->checkInput($fullname);
	$company_name=$getFromC->checkInput($company_name);
  $email=$getFromC->checkInput($email);
  $password=$getFromC->checkInput($password);

  if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      $error ="Invalid email format";
  }else if($getFromC->checkEmail($email)==true){
      $error ="email Already in Use";
  }elseif ($getFromC->checkCompanyName($company_name)==true) {
     	$error ="Company Name Already In use";
  }elseif ($getFromC->checkAdminName($fullname)==true) {
      $error="Administrator Name Already in Use";
  }else {
		$company_id =$getFromC->create('company', array('fullname'=>$fullname, 'email'=>$email,'password'=>md5($password), 'company_name'=>$company_name));
		$_SESSION['company_id'] = $company_id;
		header('Location: dash2/index.php');
  }
}
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Online Project Assistance</title>
	<link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="css/slick.css" />
	<link type="text/css" rel="stylesheet" href="css/slick-theme.css" />
	<link type="text/css" rel="stylesheet" href="css/nouislider.min.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link type="text/css" rel="stylesheet" href="css/style.css" />
</head>
<style media="screen">
	body{
		background-image: url('img/home-banner.jpg');
	}
	.container{
		margin-top: 5%;
	}
	.headtext{
		font-size: 50px;
		color: white;
		font-family: serif;
	}
  .navbar{
    background: transparent;
  }
  .subtext{

    color: white;
    font-size: 15px;
  }
</style>
<body>
	<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php" style="color:red">Online Project Assistance</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="Csignup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="Clogin.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
	<div class="container">
<div class="row">
	<div class="col-md-5 ">
		<img src="img/home-left.png" alt="" class="img-responsive">
	</div>
	<div class="col-md-6 col-md-offset-1">
<h1 class="headtext">Online Project Assistance</h1>
<h4 class="subtext">Company Signup</h4><hr>
<p class="subtext">
  	Schedule and assign task to users of your company <br> or assign taks to yourself <br>
    Let us help keep Track of your project for you <br><br> <a href="Clogin.php" style="color:red">Already Have Company Account</a>
  </p>
<form class="form-horizontal" role="form" method="post" action="Csignup.php">
<?php if(!empty($error)){
	echo'<div class="alert alert-danger">
		<p>'.$error.'</p>
	</div><br>';
}
	?>
  <div class="form-group">
    <div class="col-sm-10">
      <input type="text" name="company_name" class="form-control" id="email" placeholder="Enter company name" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-10">
      <input type="text" name="fullname" class="form-control"  placeholder="Enter fullname" required>
    </div>
  </div><div class="form-group">
    <div class="col-sm-10">
      <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-10">
      <input type="password" class="form-control" id="pwd" name="password"placeholder="Enter password" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-10">
      <div class="checkbox">
        <label style="color:white"><input type="checkbox" > Remember me</label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class=" col-sm-10">
			<input type="submit" name="submit" value="Create an Account" class="btn btn-success form-control">
    </div>
  </div>
</form>
	</div>
</div>
	</div>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/main.js"></script>

</body>

</html>

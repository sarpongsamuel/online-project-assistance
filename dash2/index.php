<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}
$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>online Project Assistance</title>
  <?php include 'partials/stylesheet.php' ?>
</head>
<body>
  <section id="container">
    <?php include 'includes/nav.php' ?>
    <?php include 'includes/aside.php' ?>
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-9 main-chart">
            <!--CUSTOM CHART START -->
            <div class="border-head">
              <h3>Project Progress</h3>
            </div>
            <div class="custom-bar-chart">
              <ul class="y-axis">
                <li><span>10.0</span></li>
                <li><span>8.0</span></li>
                <li><span>6.0</span></li>
                <li><span>4.0</span></li>
                <li><span>2.0</span></li>
                <li><span>0</span></li>
              </ul>
              <?php $getFromC->drawBarChart($company_id); ?>
            </div>
            <!--custom chart end-->
            <div class="row mt">
              <!-- SERVER STATUS PANELS -->

              <!-- /col-md-4-->
              <div class="col-md-4 col-sm-4 mb">
                <div class="darkblue-panel pn">
                  <div class="darkblue-header">
                    <h5><?=$company->company_name; ?> TASK STATICS</h5>
                  </div>
                  <canvas id="serverstatus02" height="120" width="120"></canvas>
                  <script>
                    var doughnutData = [{
                        value: <?php echo $getFromC->comp_task_done($company_id);?>,
                        color: "#1c9ca7"
                      },
                      {
                        value: 100,
                        color: "#f68275"
                      }
                    ];
                    var myDoughnut = new Chart(document.getElementById("serverstatus02").getContext("2d")).Doughnut(doughnutData);
                  </script>
                  <p><?php echo date('Y-m-d'); ?></p>

                </div>
                <!--  /darkblue panel -->
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 col-sm-4 mb">
                <!-- REVENUE PANEL -->
                <div class="green-panel pn">
                  <div class="green-header">
                    <h5><?=$company->company_name; ?> Task Created</h5>
                  </div>
                  <div class="chart mt">
                  <h1 style="font-size:50px; color:white"><b><?php echo $getFromC->all_comp_task($company_id)?></b></h1>
                  </div>
                  <!-- <p class="mt"><b><?=$company->company_name; ?></b><br/>Tasks</p> -->
                </div>
              </div>
              <div class="col-md-4 col-sm-4 mb">
                <!-- REVENUE PANEL -->
                <div class="grey-panel pn">
                  <div class="grey-header">
                    <h5><?=$company->company_name; ?> Task Completed</h5>
                  </div>
                  <div class="chart mt">
                  <h1 style="font-size:50px"><b><?php echo $getFromC->comp_task_done($company_id)?></b></h1>
                  </div>
                  <!-- <p class="mt"><b><?=$company->company_name; ?></b><br/>Tasks</p> -->
                </div>
              </div>
            </div>
            <!-- /row -->
            <div class="row">
              <!-- grey panel -->
              <div class="col-md-4 col-sm-4 mb">
                <!-- REVENUE PANEL -->
                <div class="grey-panel pn">
                  <div class="grey-header">
                    <h5><?=$company->company_name; ?> Team Members</h5>
                  </div>
                  <div class="chart mt">
                  <h1 style="font-size:60px"><b><?php echo $getFromC->getUsersCount($company_id); ?></b></h1>
                  </div>
                </div>
              </div>
              <!-- /grey-panel -->
              <!-- DIRECT MESSAGE PANEL -->
              <div class="col-md-8 mb">
                <div class="message-p pn">
                  <div class="message-header">
                    <h5>DIRECT MESSAGE FROM <?=$company->company_name; ?></h5>
                  </div>
                  <div class="row">
                    <div class="col-md-3 centered hidden-sm hidden-xs">
                      <img src="img/ui-danro.jpg" class="img-circle" width="65">
                    </div>
                    <div class="col-md-9">
                      <?php $getFromC->getLatestMessage($company_id);?>
                      <form class="form-inline" role="form" action="chat_room.php" method="post">
                        <div class="form-group">
                          <input type="text" class="form-control" id="exampleInputText" placeholder="send message" name="message">
                        </div>
                        <input type="submit" class="btn btn-default" name="submit" value="send">
                      </form>
                    </div>
                  </div>
                </div>
                <!-- /Message Panel-->
              </div>
              <!-- /col-md-8  -->
            </div>
          </div>
          <!-- /col-lg-9 END SECTION MIDDLE -->
          <!-- **********************************************************************************************************************************************************


              RIGHT SIDEBAR CONTENT
              *********************************************************************************************************************************************************** -->
          <?php include 'includes/rightbar.php'; ?>
        </div>
        <!-- /row -->
      </section>
    </section>
  <?php include 'includes/footer.php'; ?>
  </section>
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <script type="text/javascript" src="lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="lib/sparkline-chart.js"></script>
  <script src="lib/zabuto_calendar.js"></script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>

</body>
</html>

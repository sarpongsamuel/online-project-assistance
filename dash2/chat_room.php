<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}
$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);
 ?>

  <?php
  if(isset($_POST['submit'])){
   $message =$_POST['message'];
   // $file =$_POST['file'];
   $message =$getFromC->checkInput($message);
   $company_id =$company->company_id;
   $sender_id =$company->company_id;
   $sender_name =$company->fullname;
   $company_name =$company->company_name;
   $date = date('Y-m-d');
   $getFromC->create('chats', array('sender_id'=>$sender_id, 'company_id'=>$company_id,'sender_name'=>$sender_name, 'message'=>$message, 'created_at'=>$date));
   header('Location: chat_room.php');
  }
   ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>online Project Assistance</title>
  <?php include 'partials/stylesheet.php' ?>
</head>
<body>
    <?php include 'includes/nav.php' ?>
    <?php include 'includes/aside.php' ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="chat-room mt">
          <aside class="mid-side">
            <div class="chat-room-head">
              <h3>Chat Room: Support</h3>
              <form action="#" class="pull-right position" >
                <input type="text" placeholder="Search" class="form-control search-btn ">
              </form>
            </div>
            <?php $getFromC->messages($company->company_id); ?>
            <div class="group-rom last-group">
              <div class="first-part odd"></div>
              <div class="second-part"></div>
              <div class="third-part"></div>
            </div>
            <footer>
              <form class="" action="chat_room.php" method="post">
              <div class="chat-txt">
                  <input type="text" class="form-control" name="message" placeholder="message">
              </div>
              <div class="btn-group hidden-sm hidden-xs">
                <button type="button" class="btn btn-white"><i class="fa fa-meh-o"></i></button>
                <button type="button" class="btn btn-white"><i class=" fa fa-paperclip"></i></button>
              </div>
              <!-- <button class="btn btn-theme">Send</button> -->
              <input type="submit" class="btn btn-theme" name="submit" value="send">
              </form>
            </footer>
          </aside>

          <aside class="right-side">
            <div class="user-head">
              <a href="account.php" class="chat-tools btn-theme"><i class="fa fa-cog"></i> </a>
              <!-- <a href="#" class="chat-tools btn-theme03"><i class="fa fa-key"></i> </a> -->
            </div>
            <div class="invite-row">
              <h4 class="pull-left">Team Members</h4>
              <a href="newMember.php" class="btn btn-theme04 pull-right">+ member</a>
            </div>
            <ul class="chat-available-user">
              <?php $getFromC->users(); ?>
            </ul>
          </aside>
        </div>
        <!-- page end-->
      </section>
      <!-- /wrapper -->
    </section>

  <?php include 'includes/footer.php'; ?>
<?php include 'partials/javascript.php'; ?>
</body>
</html>

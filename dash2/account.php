<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}
$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);

//update company details
if(isset($_POST['submit'])){
  $company_name = $getFromC->checkInput($_POST['company_name']);
  $email        = $getFromC->checkInput($_POST['email']);
  $password     = $getFromC->checkInput($_POST['password']);
  $fullname     = $getFromC->checkInput($_POST['fullname']);

   if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      $error ="Invalid email format";
  }else if($getFromC->newUserEmail($email,$company->company_id)==true){
      $error ="email Already in Use";
   } elseif ($getFromC->newCompanyName($company_name,$company_id)==true) {
     	$error ="Company Name Already In use";
  }elseif ($getFromC->newUserName($fullname,$company_id)==true) {
      $error="Administrator Name Already in Use";
  }else {
    $getFromC->update('company', $company->company_id, array('password'=>md5($password), 'fullname'=>$fullname, 'email'=>$email,'company_name'=>$company_name));
    $_SESSION['success']= "Account details updated successfully";
    header('Location: account.php');
}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Online Project Assistance</title>
  <?php include 'partials/stylesheet.php' ?>
</head>
<body>
  <section id="container">
    <?php include 'includes/nav.php' ?>
    <?php include 'includes/aside.php' ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h1>Administrator Account</h1><hr>
              <?php if(isset($_SESSION['success'])){
                echo '<div class="alert alert-success">
                  <p>'.$_SESSION['success'].'</p>
                </div>';
              }
              ?>
              <form action="account.php" method="post"><br><br>
                <?php if(!empty($error)){
              		echo '<div class="alert alert-danger">
              			<p>'.$error.'</p>
              		</div>';
              	}
              	?>
              <div class="form-group">
              <label for="">Company Name</label>
              <input type="text" class="form-control input-lg" name="company_name" value="<?=$company->company_name;?>" required>
              </div>
              <div class="form-group">
              <label for="">FullName</label>
              <input type="text" class="form-control input-lg" name="fullname" value="<?=$company->fullname;?>" required>
              </div>
              <div class="form-group">
              <label for="">Company Email</label>
              <input type="email" class="form-control input-lg" name="email" value="<?=$company->email;?>" required>
              </div>
              <div class="form-group">
              <label for="">Company Password</label>
              <input type="password" class="form-control input-lg" name="password" value="<?=$company->password;?>" required>
              </div>
              <div class="form-group">
              <input type="submit" class="form-control input-lg btn btn-compose" name="submit" value="Update Details">
              </div>
              </form>
            </div>
            <!--  /col-lg-12 -->
          </div>
        </div>

      </section>
    </section>
  <?php include 'includes/footer.php'; ?>
  </section>
<?php include 'partials/javascript.php'; ?>
</body>
</html>

<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}
$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);

// delete a user from database
//1.delete userdata using user id
//2. delete user task from db using using user_id
if(isset($_GET['deleteUser'])){
  $user = $_GET['deleteUser'];
  $getFromC->deleteUser($company->company_id, $user);
  $getFromC->removeUserTask($company->company_id, $user);
  header('Location: members.php');
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>online Project Assistance</title>
  <?php include 'partials/stylesheet.php' ?>
</head>
<body>
  <section id="container">
    <?php include 'includes/nav.php' ?>
    <?php include 'includes/aside.php' ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row">
          <div class="col-lg-12">
            <?php $getFromC->teamMembers(); ?>
          </div>
          <!--  /col-lg-12 -->
        </div>
        <!--  /row -->
      </section>
    </section>
  <?php include 'includes/footer.php'; ?>
  </section>
<?php include 'partials/javascript.php'; ?>
</body>
</html>

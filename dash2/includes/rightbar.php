
<div class="col-lg-3 ds">
  <!--COMPLETED ACTIONS DONUTS CHART-->
  <div class="donut-main">
    <h4>COMPLETED ACTIONS & PROGRESS</h4>
    <canvas id="newchart" height="130" width="130"></canvas>
    <script>
      var doughnutData = [{
          value: <?php echo $getFromC->companyProjectCompleted($company_id); ?>,
          color: "#4ECDC4"
        },
        {
          value: <?php echo $getFromC->companyProjectCount($company_id); ?>,
          color: "#fdfdfd"
        }
      ];
      var myDoughnut = new Chart(document.getElementById("newchart").getContext("2d")).Doughnut(doughnutData);
    </script>
  </div>

  <!-- RECENT ACTIVITIES SECTION -->
  <h4 class="centered mt">RECENT ACTIVITY</h4>
  <!-- Fourth Activity -->
  <?php $getFromC->rightbarRecentProject($company_id); ?>
  <!-- USERS ONLINE SECTION -->
  <h4 class="centered mt">TEAM MEMBERS</h4>

  <!--  Members View  -->
  <?php $getFromC->rightbarMembersView(); ?>
  <a href="#" class="btn btn-default btn-block btn-sm">View All</a>
  <br>

  <!-- CALENDAR-->
  <div id="calendar" class="mb">
    <div class="panel green-panel no-margin">
      <div class="panel-body">
        <div id="date-popover" class="popover top" style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
          <div class="arrow"></div>
          <h3 class="popover-title" style="disadding: none;"></h3>
          <div id="date-popover-content" class="popover-content"></div>
        </div>
        <div id="my-calendar"></div>
      </div>
    </div>
  </div>
  <!-- / calendar -->
</div>
<!-- /col-lg-3 -->


<aside>
  <div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <p class="centered"><a href="account.php"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a></p>
      <h5 class="centered"><?= $company->fullname; ?></h5>
      <li class="mt">
        <a href="index.php">
          <i class="fa fa-dashboard"></i>
          <span><?php echo $company->company_name; ?> Dashboard</span>
          </a>
      </li>
      <li class="sub-menu">
        <a href="project.php">
          <i class="fa fa-desktop"></i>
          <span>Projects</span>
        </a>
      </li>
      <li class="sub-menu">
        <a href="javascript:;">
          <i class="fa fa-tint"></i>
          <span>Team Members</span>
          </a>
          <ul class="sub">
            <li><a href="newMember.php">Add New Member</a></li>
          </ul>
        <ul class="sub">
          <li><a href="members.php"> Members</a></li>
        </ul>
      </li>

      <li class="sub-menu">
        <a href="javascript:;">
          <i class="fa fa-comments-o"></i>
          <span>Chat Room</span>
          </a>
        <ul class="sub">
          <li><a href="chat_room.php"> Chat Room</a></li>
        </ul>
      </li>
      <li>
        <a href="account.php">
          <i class="fa fa-key"></i>
          <span>Account</span>
          </a>
      </li>
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>

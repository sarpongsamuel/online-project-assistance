<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}

$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);
 ?>
 <?php
 // get incoming project id -->
if(isset($_GET['view'])){
  $project_id= $_GET['view'];
}
// get deleted project task id -->
if(isset($_GET['delete'])){
  $task_to_delete = $_GET['delete'];
  $getFromC->deleteProjectTask($task_to_delete);
}

  //get project deatils
  $project = $getFromC->projectDetails($project_id);

  // create a new task
  if(isset($_POST['submit'])){
    $project_id = $project->project_id;
    $company_id = $company->company_id;
    //get task user name
    $task_user_id  = $_POST['task_user_id'];
    $task_user = $getFromU->userData($task_user_id);
    $task_user_name =$task_user->fullname;
    $task_to_do  = $_POST['task_to_do'];
    $task_due_date  = $_POST['task_due_date'];
    // $task_date = date('Y-m-d');
    $getFromC->create('task', array('company_id'=>$company_id, 'project_id'=>$project_id,'user_id'=>$task_user_id, 'due_date'=>$task_due_date, 'user_name'=>$task_user_name,'to_do'=>$task_to_do));
    header('Location: project.php');


  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>online Project Assistance</title>
  <?php include 'partials/stylesheet.php' ?>
</head>
<body>
    <?php include 'includes/nav.php' ?>
    <?php include 'includes/aside.php' ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
                <a href="mail_compose.html" class="btn btn-compose">
                  <i class="fa fa-suitcase"></i><?php echo $project->project_name; ?>
                  </a>
                  <!-- create a new task panel -->
                      <ul class="nav nav-pills nav-stacked labels-info ">
                        <li>
                          <h4 class="text-center" style="font-weight:bold">Create New Task</h4>
                        </li>
                        <form class="" action="" method="post">
                          <div class="form-group">
                            <label for="">Select User</label>
                            <select class="form-control" name="task_user_id" required>
                              <?php $getFromC->assignUserTask($company_id,$project_id); ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="">Task Due Date</label>
                            <input type="date" class="form-control" name="task_due_date" value="" required>
                          </div>
                          <div class="form-group">
                            <label for="">Task Todo</label>
                            <textarea name="task_to_do" rows="3" cols="80" class="form-control" placeholder="enter task here.."required></textarea>
                          </div>
                          <div class="form-group">
                            <input type="submit" name="submit" value="create new task" class="form-control btn btn-compose btn-xs">
                          </div>
                        </form>
                      </ul>
              </div>
            </section>
            <section class="panel">
              <div class="panel-body">
                <ul class="nav nav-pills nav-stacked labels-info ">
                  <li>
                    <h4>workers Under Project</h4>
                  </li>
                  <?php $getFromC->companyProjectUsers($company_id, $project_id); ?>

                </ul>
                <a href="newMember.php"> + Add More</a>

              </div>
            </section>

          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Tasks
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
                <div class="mail-option">

                  <div class="btn-group">
                    <a data-original-title="Refresh" data-placement="top" data-toggle="dropdown" href="#" class="btn mini tooltips">
                      <i class=" fa fa-refresh"></i>
                      </a>
                  </div>

                  <ul class="unstyled inbox-pagination">
                    <li><span>1-50 of 99</span></li>
                    <li>
                      <a class="np-btn" href="#"><i class="fa fa-angle-left  pagination-left"></i></a>
                    </li>
                    <li>
                      <a class="np-btn" href="#"><i class="fa fa-angle-right pagination-right"></i></a>
                    </li>
                  </ul>
                </div>
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                    <?php $getFromC->projectTask($company_id,$project_id); ?>
                    </tbody>
                  </table>
                </div>

              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
        <!-- page end-->
      </section>
      <!-- /wrapper -->
    </section>

  <?php include 'includes/footer.php'; ?>
<?php include 'partials/javascript.php'; ?>
</body>
</html>

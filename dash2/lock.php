<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}
$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);

if(isset($_POST['submit'])){
  $company_id =@$_SESSION['company_id'];
  $company = $getFromC->companyData($company_id);

  $password = $_POST['password'];
  // encrypt password with md5
  // $password = md5($password);
  // get current logged in details
  $company_email =$company->email;
  $getFromC->login($company_email, $password);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>online Project Assistance</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body onload="getTime()">
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
  <div class="container">
    <div id="showtime"></div>
    <div class="col-lg-4 col-lg-offset-4">
      <div class="lock-screen">
        <h2><a data-toggle="modal" href="#myModal"><i class="fa fa-lock"></i></a></h2>
        <p>UNLOCK</p>
        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Welcome Back</h4>
              </div>
              <div class="modal-body">
                <p class="centered"><img class="img-circle" width="80" src="img/ui-sam.jpg"></p>
                <form class="" action="lock.php" method="post">
                  <div class="form-group">
                    <label for="">Enter Password</label>
                    <input type="password" name="password" value="" class="form-control input-md" placeholder="password here.." required>
                  </div>
                  <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-theme03 " value="unlock">

                  </div>
                </form>
              </div>
              <!-- <div class="modal-footer centered"> -->
                <!-- <button class="btn btn-theme03 button" type="button" > <a href="">unlock</a></button> -->
                <!-- <input type="submit" name="submit" class="btn btn-theme03 " value="unlock">
              </div> -->
            </div>
          </div>
        </div>
        <!-- modal -->
      </div>
      <!-- /lock-screen -->
    </div>
    <!-- /col-lg-4 -->
  </div>
  <!-- /container -->
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="lib/jquery.backstretch.min.js"></script>
  <script>
  $(document).ready(function(){

    $('.button').click(function(){
      $('form').submit();
    });

    });
    $.backstretch("img/login-bg1.jpg", {
      speed: 500
    });
  </script>
  <script>
    function getTime() {
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();
      // add a zero in front of numbers<10
      m = checkTime(m);
      s = checkTime(s);
      document.getElementById('showtime').innerHTML = h + ":" + m + ":" + s;
      t = setTimeout(function() {
        getTime()
      }, 500);
    }

    function checkTime(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
  </script>
</body>

</html>

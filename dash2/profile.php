<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}
$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);
 ?>
<?php if(isset($_GET['user'])){
 $user= $_GET['user'];
 $userDetail = $getFromU->userData($user);
 $pDetails =$getFromC->projectDetails($userDetail->project_id);
}
?>

<!-- delete task -->
<?php if(isset($_GET['delete'])){
  $taskID =$_GET['delete'];
  $getFromU->deleteUserTask($taskID);
} ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>OPA</title>
  <?php include 'partials/stylesheet.php' ?>
  <link rel="stylesheet" href="css/to-do.css">

</head>
<body>
  <section id="container">
    <?php include 'includes/nav.php' ?>
    <?php include 'includes/aside.php' ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="col-lg-12">
            <div class="row content-panel">
              <div class="col-md-4 profile-text mt mb centered">
                <div class="right-divider hidden-sm hidden-xs">
                  <h4><?php echo $getFromU->totalUserTask($userDetail->user_id,$company_id); ?></h4>
                  <h6>TASK ASSIGNED</h6>
                  <h4><?php echo $getFromU->userCompletedTask($user,$company_id); ?></h4>
                  <h6>TASK COMPLETED</h6>
                  <h4><?php echo $getFromU->UserIncompleteTask($user,$company_id); ?></h4>
                  <h6>TASK PENDING</h6>
                </div>
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 profile-text">
                <h3><?php echo $userDetail->fullname; ?></h3>
                <h6><?php echo $userDetail->work; ?></h6>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.</p>
                <br>
                <!-- <p><button class="btn btn-theme"><i class="fa fa-envelope"></i> Send Message</button></p> -->
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 centered">
                <div class="profile-pic">
                  <p><img src="img/friends/fr-05.jpg" class="img-circle"></p>
                  <!-- <p>
                    <button class="btn btn-theme"><i class="fa fa-check"></i> Follow</button>
                    <button class="btn btn-theme02">Add</button>
                  </p> -->
                </div>
              </div>
              <!-- /col-md-4 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /col-lg-12 -->
          <div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a data-toggle="tab" href="#overview">Overview</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#contact" class="contact-map">Contact</a>
                  </li>
                  <!-- <li>
                    <a data-toggle="tab" href="#edit">Edit Profile</a>
                  </li> -->
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="overview" class="tab-pane active">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="detailed mt-0">
                          <h4>Project Workings</h4>
                          <div class="recent-activity">
                            <div class="activity-icon bg-theme"><i class="fa fa-check"></i></div>
                            <div class="activity-panel">
                              <!-- <h5><?php echo  $pDetails->project_name ?></h5> -->
                              <div class="panel-group" id="accordion">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><?php echo  $pDetails->project_name ?></a>
                                  </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse " aria-expanded="false">
                                  <div class="panel-body">
                                    <section class="task-panel tasks-widget">
                                      <div class="panel-heading">
                                        <div class="pull-left">
                                          <h5><i class="fa fa-tasks"></i>Task to-do</h5>
                                        </div>
                                        <br>
                                      </div>
                                      <div class="panel-body">
                                        <div class="task-content">
                                          <ul id="sortable" class="task-list">
                                            <?php $getFromU->userTask($user,$company_id); ?>
                                          </ul>
                                          </div>
                                        </div>
                                      </section>
                                </div>
                                </div>
                              </div>
                            </div>
                            </div>
                          </div>
                          <!-- /recent-activity -->
                        </div>
                        <!-- /detailed -->
                      </div>
                      <!-- /col-md-6 -->
                      <div class="col-md-6 detailed">
                        <h4>User Stats</h4>
                        <div class="row centered mt mb">
                          <div class="col-sm-4">
                            <h1><i class="fa fa-money"></i></h1>
                            <h3><?php echo $getFromU->totalUserTask($user,$company_id); ?></h3>
                            <h6>TOTAL TASK ASSIGNED</h6>
                          </div>
                          <div class="col-sm-4">
                            <h1><i class="fa fa-trophy"></i></h1>
                            <h3><?php echo $getFromU->userCompletedTask($user,$company_id); ?></h3>
                            <h6>COMPLETED TASKS</h6>
                          </div>
                          <div class="col-sm-4">
                            <h1><i class="fa fa-folder-open"></i></h1>
                            <h3> <?php echo $getFromU->UserIncompleteTask($user,$company_id); ?> </h3>
                            <h6>INCOMPLETED TASK</h6>
                          </div>
                        </div>
                        <h4>Tasks Progress</h4>
                        <div class="row centered">
                          <div class="col-md-8 col-md-offset-2">
                            <h5>Total Task <?php echo $getFromU->totalUserTask($user,$company_id); ?>%</h5>
                            <div class="progress">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $getFromU->totalUserTask($user,$company_id); ?>%">
                                <span class="sr-only"><?php echo $getFromU->totalUserTask($user,$company_id); ?></span>
                              </div>
                            </div>
                            <h5>Completed Task <?php echo $getFromU->userCompletedTask($user,$company_id); ?>%</h5>
                            <div class="progress">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $getFromU->userCompletedTask($user,$company_id); ?>%">
                                <span class="sr-only"><?php echo $getFromU->userCompletedTask($user,$company_id); ?></span>
                              </div>
                            </div>
                            <h5>Incomplete Task <?php echo $getFromU->UserIncompleteTask($user,$company_id); ?>%</h5>
                            <div class="progress">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $getFromU->UserIncompleteTask($user,$company_id); ?>%">
                                <span class="sr-only"><?php echo $getFromU->UserIncompleteTask($user,$company_id); ?></span>
                              </div>
                            </div>
                          </div>
                          <!-- /col-md-8 -->
                        </div>
                        <!-- /row -->
                      </div>
                      <!-- /col-md-6 -->
                    </div>
                    <!-- /OVERVIEW -->
                  </div>
                  <!-- /tab-pane -->
                  <div id="contact" class="tab-pane">
                    <div class="row">
                      <div class="col-md-6">
                        <div id="map"></div>
                      </div>
                      <div class="col-md-6 detailed">
                        <h4>Contacts</h4>
                        <div class="col-md-8 col-md-offset-2 mt">
                          <p>
                            Fullname: <?php echo $userDetail->fullname; ?>
                          </p>
                          <br>
                          <p>
                            Email: <?php echo $userDetail->email; ?>
                          </p>
                        </div>
                      </div>
                      <!-- /col-md-6 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->

                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
          <!-- /row -->
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
  </section>
  <?php include 'includes/footer.php'; ?>
<?php include 'partials/javascript.php'; ?>
</body>
</html>

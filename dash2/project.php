<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}
$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);
 ?>

  <?php
  if(isset($_POST['submit'])){
   $project_name =$_POST['project_name'];
   $project_name =$getFromC->checkInput($project_name);
   $company_id =$company->company_id;
   $company_name =$company->company_name;
   $date = date('Y-m-d');
   $getFromC->create('project', array('company_id'=>$company_id, 'company_name'=>$company_name,'project_name'=>$project_name, 'created_at'=>$date));
   header('Location: project.php');
  }
   ?>

   <!-- delete project from database -->
   <?php
    if(isset($_GET['delete'])){
      $del =$_GET['delete'];
      $getFromC->removeProject($del,$company_id);
    }


    ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>online Project Assistance</title>
  <?php include 'partials/stylesheet.php' ?>
</head>
<body>
    <?php include 'includes/nav.php' ?>
    <?php include 'includes/aside.php' ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="chat-room mt">
          <aside class="mid-side">
            <div class="chat-room-head">
              <h3> Project</h3>
              <form action="#" class="pull-right position" >
                <input type="text" placeholder="Search" class="form-control search-btn ">
              </form>
            </div>
            <div class="invite-row">
              <h4 class="pull-left">Working Projects</h4>
              <!-- <a href="#" class="btn btn-theme04 pull-right">+ Invite</a> -->
            </div>
            <!-- project dispay here -->
            <?php $getFromC->companyProject($company_id); ?>

          </aside>

          <aside class="right-side">
            <div class="user-head">
              <a href="#" class="chat-tools btn-theme"><i class="fa fa-cog"></i> </a>
              <a href="#" class="chat-tools btn-theme03"><i class="fa fa-key"></i> </a>
            </div>
            <div class="invite-row">
              <h4 class="pull-left">Add New Project</h4>
              <!-- <a href="#" class="btn btn-theme04 pull-right">+ Invite</a> -->
            </div>
            <ul class="chat-available-user">
            <form class="" action="project.php" method="post">
              <label for=""> <strong>Project Name</strong> </label>
              <input type="text" class="form-control" name="project_name" value="" required><br>
              <input type="submit" name="submit" class="form-control btn btn-primary" value="Add new Project">
            </form>
            </ul>
          </aside>
        </div>
        <!-- page end-->
      </section>
      <!-- /wrapper -->
    </section>

  <?php include 'includes/footer.php'; ?>
<?php include 'partials/javascript.php'; ?>
</body>
</html>

<link href="img/favicon.png" rel="icon">
<link href="img/apple-touch-icon.png" rel="apple-touch-icon">
<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/zabuto_calendar.css">

<link rel="stylesheet" type="text/css" href="lib/gritter/css/jquery.gritter.css" />

<script src="lib/chart-master/Chart.js"></script>

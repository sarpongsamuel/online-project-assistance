<?php include '../core/init.php'; ?>
<?php
if($getFromC->loggedIn() === false){
  $getFromC->userRedirect();
}
$company_id =@$_SESSION['company_id'];
$company = $getFromC->companyData($company_id);
?>

<!-- insert new users into db  -->
<?php
if(isset($_POST['submit'])){
$fullname =$_POST['fullname'];
$email =$_POST['email'];
$work =$_POST['work'];
$todo =$_POST['to_do'];
$project_id =$_POST['project_id'];

$fullname=$getFromC->checkInput($fullname);
$email=$getFromC->checkInput($email);
$work=$getFromC->checkInput($work);
$todo=$getFromC->checkInput($todo);

//company id
$company_id =$_SESSION['company_id'];

if(!empty($email)){
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      $error ="Invalid email format";
    }else if($getFromC->newUserEmail($email,$company_id)=== true){
        $error ="Email already taken by another user ";
     }else{
    $new_user_id=$getFromU->create('users', array('fullname'=>$fullname, 'email'=>$email,'work'=>$work, 'company_id'=>$company_id, 'project_id'=>$project_id));
    $new_user_data = $getFromU->userData($new_user_id);
    $task = $getFromU->create('task', array('company_id'=>$company_id, 'user_id'=>$new_user_id,'user_name'=>$new_user_data->fullname,'to_do'=>$todo, 'project_id'=>$project_id));

    // send mail to user
    $subject =$company->company_name;
    $mailFrom=$company->email;

    $date = date('Y-m-d');
    $mailTo =$new_user_data->email;
    $message ="You have been Added to .$subject. Project by .$mailFrom. on .$date.";
    mail($mailTo, $subject, $message,$mailFrom);
    header('Location: members.php');

  }
 }
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>online Project Assistance</title>
  <?php include 'partials/stylesheet.php' ?>
</head>
<body>
  <section id="container">
    <?php include 'includes/nav.php' ?>
    <?php include 'includes/aside.php' ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-lg-12">
              <div class="form-panel">
              <?php
               if(isset($error)){
                 echo '
                 <div class="alert alert-danger">
                 <p>'.$error.' </p>
                 </div>';
               }

              ?>
                <form action="newMember.php" class="form-horizontal style-form" method="post">
                  <h1>Add New User Account</h1> <hr>
                  <div class="form-group">
                    <label class="control-label col-md-3">Enter Fullname here</label>
                    <div class="col-md-3 col-xs-11">
                      <input class="form-control form-control-inline input-large " size="16" type="text" value="" name="fullname" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Enter Email here</label>
                    <div class="col-md-3 col-xs-11">
                      <input class="form-control form-control-inline input-large " size="16" type="email" value="" name="email" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Enter User's Work</label>
                    <div class="col-md-3 col-xs-11">
                      <input class="form-control form-control-inline input-large" size="16" type="text" value="" name="work" placeholder="eg  graphics manager" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Assign Project To User</label>
                    <div class="col-md-3 col-xs-11">
                      <select class="form-control" name="project_id" required>
                        <?php $getFromC->companyProjectList($company_id); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Task For User</label>
                    <div class="col-md-3 col-xs-11">
                      <textarea  rows="5" cols="80" class="form-control form-control-inline input-large" name="to_do" required></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"></label>
                    <div class="col-md-3 col-xs-11">
                      <input class="form-control form-control-inline btn btn-info" size="16" type="submit" value="Add New User" name="submit">
                    </div>
                  </div>
                </form>
              </div>
              <!-- /form-panel -->
            </div>

          </div>
          <!--  /col-lg-12 -->
        </div>
        <!--  /row -->
      </section>
    </section>
  <?php include 'includes/footer.php'; ?>
  </section>
<?php include 'partials/javascript.php'; ?>
</body>
</html>

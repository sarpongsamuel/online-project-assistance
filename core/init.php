<?php
include 'database/connection.php';
// include 'classes/general.php';
include 'classes/company.php';
include 'classes/user.php';

global $pdo;

// creating objects of each classes so that they can access pdo in connection.php
// $getFromG = new General($pdo);
$getFromC = new Company($pdo);
$getFromU = new User($pdo);

// our session
session_start();

// our constraint
define("BASE_URL","http://localhost:8080/atom/OPA/");
?>

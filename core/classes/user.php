<?php
class User{
  // $pdo is  accessible in user  class other classes that inherits the User Class only
  protected $pdo;

  // creating a constructor to access the pdo object in connection.php
  function __construct($pdo){
    $this->pdo = $pdo;
  }
//sanitize user input
public function checkInput($var){
  $var =htmlspecialchars($var);
  $var =trim($var);
  $var =stripslashes($var);
  return $var;
}

// public function search($search){
//   $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `completed`=1");
//   $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
//   $stmt->execute();
//
// }

//check if user email exsit in the DB
public function validateEmail($email){
    $stmt =$this->pdo->prepare("SELECT `email` FROM users WHERE `email`=:email ");
    $stmt->bindParam(":email",$email,PDO::PARAM_STR);
    $stmt->execute();
    $count =$stmt->rowCount();
    if($count > 0){
        return true;
    }else{
      return false;
  }
}

//prevent user from updating email to an existing mail in users table
  public function checkEmail($email,$user_id){
      $stmt =$this->pdo->prepare("SELECT `email` FROM users WHERE `email`=:email AND `user_id` !=:user_id");
      $stmt->bindParam(":email",$email,PDO::PARAM_STR);
      $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
      $stmt->execute();
      $count =$stmt->rowCount();
      if($count > 0){
          return true;
      }else{
        return false;
    }
  }

  //step1 id to continue
  public function step1($email){
    $stmt =$this->pdo->prepare("SELECT * FROM users WHERE `email`=:email");
    $stmt->bindParam(":email",$email,PDO::PARAM_STR);
    $stmt->execute();
    $step1 =$stmt->fetch(PDO::FETCH_OBJ);
    return $step1->user_id;
  }
//user login function
    public function login($email,$password){
    $stmt =$this->pdo->prepare("SELECT `user_id` FROM `users` WHERE `email`=:email AND `password`=:password ");
    $stmt->bindParam(":email", $email, PDO::PARAM_STR);
    $password = md5($password);
    $stmt->bindParam(":password", $password, PDO::PARAM_STR);
    $stmt->execute();

    //similar to mysqli_fetch _assoc()
    $user =$stmt->fetch(PDO::FETCH_OBJ);
    $count =$stmt->rowCount();

    if($count > 0){
      $_SESSION['user_id'] = $user->user_id;
      header('Location: dashboard');
    }else{
      return false;
    }
  }

  // check if user id logged in
  public function loggedIn(){
    return (isset($_SESSION['user_id'])) ?true : false;
  }

  //user logout function
  public function logout(){
    $_SESSION = array();
    session_destroy();
    header('Location: '.BASE_URL.'index.php');
  }

  // redirect function
  public function userRedirect(){
    header('Location: ../login.php');
  }

//insert method
  public function create($table, $fields=array()){
    $column = implode(',', array_keys($fields));
    $values =':' .implode(', :', array_keys($fields));
    $sql ="INSERT INTO {$table} ({$column}) VALUES ({$values})";
    if($stmt = $this->pdo->prepare($sql)){
      foreach ($fields as $key => $data) {
        $stmt->bindValue(':' .$key, $data);
      }
      $stmt->execute();
      return $this->pdo->lastInsertId();
    }
  }

  //update method
  public function update($table, $user_id,$fields=array()){
    $columns ='';
    $i=1;
    foreach ($fields as $name => $value){
      $columns .="`{$name}` = :{$name}";
      if($i < count($fields)){
        $columns .=',  ';
      }
      $i++;
    }
    $sql ="UPDATE {$table} SET {$columns} WHERE `user_id`= {$user_id}";
    if($stmt = $this->pdo->prepare($sql)){
      foreach ($fields as $key => $value) {
        $stmt->bindValue(':'.$key,$value);
      }
    $stmt->execute();
    }
  }

  ///a time ago function for task
  public function timeAgo($datetime){
    $time = strtotime($datetime);
    $current = time();
    $seconds = $current - $time;
    $minutes = round($seconds / 60);
    $hours = round($seconds / 3600);
    $months = round($seconds/2600640);

    if($seconds  <= 60){
      if($seconds == 0){
        return 'now';
      }
    }elseif ($minutes <= 60) {
      return $minutes."m";
    }elseif ($hours <= 24) {
      return $hours."h";
    }elseif ($months <= 12) {
       return date('M j', $time);
    }else{
      return date('j M Y', $time);
    }
  }


//--------------------------------------------------------ALL COMPANY AND USER QUERIES DOWN HERE----------------------------------------------------------------------------------------------------
// get listing of users task
public function userTask($user_id,$company_id){
  $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `company_id`=:company_id AND `user_id`=:user_id");
  $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
  $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
  $stmt->execute();
  while($task =$stmt->fetch(PDO::FETCH_OBJ)){
    echo '<li class="list-primary">
      <i class=" fa fa-ellipsis-v"></i>
      <div class="task-checkbox">
        <input type="checkbox" class="list-child" value="" />
      </div>
      <div class="task-title">
        <span class="task-title-sp">'.$task->to_do.'</span>
        <span class="badge bg-theme ">'.$this->timeAgo($task->created_at). '</span>
        <div class="pull-right hidden-phone">
          <a href="profile.php?delete='.$task->task_id.'" class="btn btn-danger btn-xs fa fa-trash-o"></a>
        </div>
      </div>
    </li>
    ';
  }
}
//delete a user task
public function deleteUserTask($task_id){
  $stmt =$this->pdo->prepare("DELETE FROM `task` WHERE `task_id`=:task_id");
  $stmt->bindParam(":task_id",$task_id,PDO::PARAM_STR);
  $stmt->execute();
  header('Location: members.php');
}

      //fetch userdata
      public function userData($user_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `users` WHERE `user_id`=:user_id");
        $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
      }
      // projects copleted by user under a company
      public function userCompletedTask($user_id,$company_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `company_id`=:company_id AND `completed` =1");
        $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
        $count =$stmt->rowCount();
        return $count;
      }
      // projects copleted by user under a company
      public function totalUserTask($user_id,$company_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `company_id`=:company_id");
        $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
        $count =$stmt->rowCount();
        return $count;
      }
      //get user completed task under each company
      public function UserIncompleteTask($user,$company_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `company_id`=:company_id AND `completed`= 0");
        $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
        $count =$stmt->rowCount();
        return $count;
      }

      // GET USER COMPANY name
      public function getCompanyName($company_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `company` WHERE `company_id`=:company_id ");
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
        $company =$stmt->fetch(PDO::FETCH_OBJ);
        return $company->company_name;
      }

      //company listing under mycompany on aside.php
    public function userCompany($user_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `users` WHERE `user_id` =:user_id");
    $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
    $stmt->execute();
    while($company =$stmt->fetch(PDO::FETCH_OBJ)){
     echo '
     <li><a href="company.php?company_id='.$company->company_id.'">'.$this->getCompanyName($company->company_id).'</a></li>';
    }
  }
  //get user project name
  public function getProjectName($project_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `project` WHERE `project_id`=:project_id ");
    $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
    $stmt->execute();
    $project =$stmt->fetch(PDO::FETCH_OBJ);
    return $project->project_name;

}

//display user task under company.php file
  public function getUserTask($user_id,$company_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `company_id`=:company_id AND `completed`=0");
    $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
    $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
    $stmt->execute();
    // $count = $stmt->rowCount();
    // if($count = 0){
    //   echo ' <h1 style="margin-top:100px">No Task Available Now!</h1>';
    // }
    while($task =$stmt->fetch(PDO::FETCH_OBJ)){
     echo '
     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
       <div class="box-part text-center">
       <i class="fa fa-renren fa-3x" aria-hidden="true"></i>
         <div class="title">
           <h4>'.$this->getProjectName($task->project_id).'</h4>
         </div>
         <div class="text">
           <span>'.$task->to_do.'</span>
         </div>
         <a >Date Due: '.$task->due_date.'</a><br><br>
         <a href="task.php?complete='.$task->task_id.'" class="btn btn-default">Complete</i></a>
       </div>
     </div>
     ';
    }
  }

  //get all task for user and display at individual/index.php
  public  function myTask($user_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id LIMIT 3");
    $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
    $stmt->execute();
    while($task =$stmt->fetch(PDO::FETCH_OBJ)){
      echo '<div class="recent-activity">
        <div class="activity-icon bg-theme04"><i class="fa fa-rocket"></i></div>
        <div class="activity-panel">
          <h5>'.$this->getCompanyName($task->company_id).'</h5>
          <p>'.$task->to_do.'.</p>
        </div>
      </div>
      ';
    }
  }
  //pending task for header.php dropdown
  public function pendingTask($user_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `completed`=0 LIMIT 3");
    $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
    $stmt->execute();
    while($task = $stmt->fetch(PDO::FETCH_OBJ)){
      echo '<li>
        <a>
          <div class="task-info">
            <div class="desc">'.$task->to_do.'</div>
          </div>
        </a>
      </li>
      ';
    }
  }

  //get all task completed by irrespective of company
  public function myTaskCompleted($user_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `completed`=1");
    $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
    $stmt->execute();
    $count =$stmt->rowCount();
    return $count;
  }
  //get all task unfinised by irrespective of company
  public function myTaskUnfinished($user_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `completed`=0");
    $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
    $stmt->execute();
    $count =$stmt->rowCount();
    return $count;
  }
  //get all task assigned to user irrespective of company
  public function myTaskCount($user_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id ");
    $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
    $stmt->execute();
    $count =$stmt->rowCount();
    return $count;
  }
  //list all completed taskk on task.php for each user
  public function taskCompleted($user_id){
    $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `user_id` =:user_id AND `completed`=1");
    $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
    $stmt->execute();
    while($task =$stmt->fetch(PDO::FETCH_OBJ)){
      echo '<tr class="unread">
        <td class="inbox-small-cells">
          <input type="checkbox" class="mail-checkbox">
        </td>
        <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
        <td class="view-message dont-show"><a href="">'.$this->getCompanyName($task->company_id).'</a></td>
        <td class="view-message dont-show"><a href="">'.$this->getProjectName($task->project_id).'</a></td>
        <td class="view-message"><a href="">'.$task->to_do.'</a></td>
        <td class="view-message inbox-small-cells">'.$task->created_at.'</td>
        <td class="view-message ">'.$task->due_date.'</td>
      </tr>';
    }
  }
  public function updateTask($task_id){
    $stmt =$this->pdo->prepare("UPDATE `task` SET `completed`='1'  WHERE `task_id` =:task_id");
    $stmt->bindParam(":task_id",$task_id,PDO::PARAM_STR);
    $stmt->execute();
  }

  public function taskTimeOut($company_id){
    $stmt =$this->pdo->prepare("SELECT `due_date`FROM `task` WHERE `company_id`=:company_id ");
    $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
    $stmt->execute();
    $dueDate =$stmt->fetch(PDO::FETCH_OBJ);

    foreach ($dueDate as $dueDate) {
      // code..
      $currentDate= date('Y-m-d');
      $newDate = date_create('2019-07-07');
      $diff = $currentDate-$newDate;
      echo $diff;
    }

}

}?>

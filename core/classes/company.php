<?php
class Company{
  // $pdo is  accessible in user  class other classes that inherits the User Class only
  protected $pdo;
  // creating a constructor to access the pdo object in connection.php
  function __construct($pdo){
    $this->pdo = $pdo;
  }



// <<<<--------------------------------All validations for company -------------------------------------->>>>
public function checkInput($var){
  $var =htmlspecialchars($var);
  $var =trim($var);
  $var =stripslashes($var);
  return $var;
}

  //check if company_name exsit in the DB
    public function checkCompanyName($company_name){
        $stmt =$this->pdo->prepare("SELECT `company_id`,`company_name` FROM company WHERE `company_name`=:company_name");
        $stmt->bindParam(":company_name",$company_name,PDO::PARAM_STR);
        $stmt->execute();
        $count =$stmt->rowCount();
        if($count > 0){
            return true;
        }else{
          return false;
        }
    }

    //check if user email exsit in the DB
      public function checkEmail($email){
          $stmt =$this->pdo->prepare("SELECT `email` FROM company WHERE `email`=:email");
          $stmt->bindParam(":email",$email,PDO::PARAM_STR);
          $stmt->execute();
          $count =$stmt->rowCount();
          if($count > 0){
              return true;
          }else{
            return false;
        }
      }

      //prevent user from updating email to an existing mail in company table
        public function newUserEmail($email,$company_id){
            $stmt =$this->pdo->prepare("SELECT `email` FROM company WHERE `email`=:email");
            $stmt->bindParam(":email",$email,PDO::PARAM_STR);
            // $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
            $stmt->execute();
            $count =$stmt->rowCount();
            if($count > 0){
                return true;
            }else{
              return false;
          }
        }

        //prevent user from updating fullname to an existing fullname in company table
          public function newUserName($fullname,$company_id){
              $stmt =$this->pdo->prepare("SELECT `fullname` FROM company WHERE `fullname`=:fullname AND `company_id` !=:company_id");
              $stmt->bindParam(":fullname",$fullname,PDO::PARAM_STR);
              $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
              $stmt->execute();
              $count =$stmt->rowCount();
              if($count > 0){
                  return true;
              }else{
                return false;
            }
          }

          //prevent user from updating companyName to an existing name  in company table
            public function newCompanyName($company_name,$company_id){
                $stmt =$this->pdo->prepare("SELECT `company_name` FROM company WHERE `company_name`=:company_name AND `company_id` !=:company_id");
                $stmt->bindParam(":company_name",$company_name,PDO::PARAM_STR);
                $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
                $stmt->execute();
                $count =$stmt->rowCount();
                if($count > 0){
                    return true;
                }else{
                  return false;
              }
            }

          // prevent user from registering with an existing manager fullname
          public function checkAdminName($fullname){
          $stmt =$this->pdo->prepare("SELECT `fullname` FROM company WHERE `fullname`=:fullname");
          $stmt->bindParam(":fullname",$fullname,PDO::PARAM_STR);
          $stmt->execute();
          $count =$stmt->rowCount();
          if($count > 0){
              return true;
          }else{
            return false;
        }
      }

        public function login($email,$password){
        $stmt =$this->pdo->prepare("SELECT `company_id` FROM `company` WHERE `email`=:email AND `password`=:password ");
        $stmt->bindParam(":email", $email, PDO::PARAM_STR);
        $password = md5($password);
        $stmt->bindParam(":password", $password, PDO::PARAM_STR);
        $stmt->execute();

        //similar to mysqli_fetch _assoc()
        $company =$stmt->fetch(PDO::FETCH_OBJ);
        $count =$stmt->rowCount();

        if($count > 0){
          @$_SESSION['company_id'] = $company->company_id;
          header('Location: '.BASE_URL.'dash2/index.php');
        }else{
          return false;
        }
      }

      // check if user id logged in
      public function loggedIn(){
        return (isset($_SESSION['company_id'])) ?true : false;
      }
      //user logout function
      public function logout(){
        $_SESSION = array();
        session_destroy();
        header('Location: '.BASE_URL.'index.php');
      }

      // redirect function
      public function userRedirect(){
        header('Location: ../index.php');
      }

      //prevent user from accessing php files
      public function preventAccess($request, $currentFile, $currently){
        if($request== "GET" && $currentFile == $currently ){
          header('Location: index.php');
        }
      }



// <<<<---------------------------------------------CRUD------------------------------------------------------------------------>>>>

public function create($table, $fields=array()){
  $column = implode(',', array_keys($fields));
  $values =':' .implode(', :', array_keys($fields));
  $sql ="INSERT INTO {$table} ({$column}) VALUES ({$values})";
  if($stmt = $this->pdo->prepare($sql)){
    foreach ($fields as $key => $data) {
      $stmt->bindValue(':' .$key, $data);
    }
    $stmt->execute();
    return $this->pdo->lastInsertId();
  }
}

//update method
public function update($table, $company_id,$fields=array()){
  $columns ='';
  $i=1;
  foreach ($fields as $name => $value){
    $columns .="`{$name}` = :{$name}";
    if($i < count($fields)){
      $columns .=',  ';
    }
    $i++;
  }
  $sql ="UPDATE {$table} SET {$columns} WHERE `company_id`= {$company_id}";
  if($stmt = $this->pdo->prepare($sql)){
    foreach ($fields as $key => $value) {
      $stmt->bindValue(':'.$key,$value);
    }
  $stmt->execute();
  }
}


// <<<<---------------------------------COMPANY AND Dashboard fUNCTIONS---------------------------------->>>>
      //fetch company details
      public function companyData($company_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `company` WHERE `company_id`=:company_id");
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
      }

      //lock screen Method
      public function lockScreen(){
        $stmt =$this->pdo->prepare("DELETE FROM `project` WHERE `project_id`=:project_id AND `company_id`=:company_id");
        $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
      }
      // get company company messages
      public function messages($company_id){
        $stmt =$this->pdo->prepare("SELECT * FROM chats WHERE `company_id`=:company_id");
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
        while($message =$stmt->fetch(PDO::FETCH_OBJ)){
          echo '<div class="group-rom">
            <div class="first-part odd">'.$message->sender_name.'</div>
            <div class="second-part">'.$message->message.'</div>
            <div class="third-part">'.$message->created_at.'</div
          </div>';
        }
    }

    ///company latest message for index.php of company page
    public function getLatestMessage($company_id){
      $sender_id =$company_id;
      $stmt =$this->pdo->prepare("SELECT * FROM chats WHERE	`id`=(SELECT max(id) FROM chats WHERE `company_id`=:company_id)");
      $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
      // $stmt->bindValue(":sender_id",$sender_id,PDO::PARAM_STR);
      $stmt->execute();
      while($msg =$stmt->fetch(PDO::FETCH_OBJ)){
        echo '<p>
        <name>'.(($msg->sender_id == $company_id)?'You':$msg->sender_name).'</name>
        sent a message.
        </p>
        <p class="small">'.$msg->created_at.'</p>
        <p class="message">'.$msg->message.'</p>';
      }

    }




// <<<<--------------------------------ALl PROJECT fUNCTIONS-------------------------------------->>>>


public function removeProject($project_id,$company_id){
    // delete product from database
      $stmt =$this->pdo->prepare("DELETE FROM `project` WHERE `project_id`=:project_id AND `company_id`=:company_id");
      $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
      $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
      $stmt->execute();

      // delete all project task
      $stmt =$this->pdo->prepare("DELETE FROM `task` WHERE `project_id`=:project_id AND `company_id`=:company_id");
      $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
      $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
      $stmt->execute();
      header('Location: project.php');
    }

      // projects to assign to users/workers when creatinga new user
      public function companyProjectList($company_id){
        $pID =$company_id;
        $stmt =$this->pdo->prepare("SELECT * FROM project WHERE `company_id`= $pID");
        $stmt->bindValue(":company_id",$pID,PDO::PARAM_STR);
        $stmt->execute();
        while($pname =$stmt->fetch(PDO::FETCH_OBJ)){
          echo '
              <option value="'.$pname->project_id.'">'.$pname->project_name.'</option>
          ';
        }
      }

      //users under each company project
      public function companyProjectUsers($company_id,$project_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `users` WHERE `company_id`=:company_id AND `project_id`=:project_id");
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
        $stmt->execute();
        while($user =$stmt->fetch(PDO::FETCH_OBJ)){
          echo '
          <li>
            <a href="#">
                <img src="img/friends/fr-10.jpg" class="img-circle" width="20">'.$user->fullname.'
                <p><span class="label label-success">'.$user->email.'</span></p>
              </a>
          </li>
          ';
        }
      }
      //count task completed  for each company forindex page widget
      public function comp_task_done($company_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `company_id`=:company_id AND `completed`=1");
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
        $count = $stmt->rowCount();
        return $count;
      }

      //count task completed  for each company
      public function all_comp_task($company_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `company_id`=:company_id ");
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->execute();
        $count = $stmt->rowCount();
        return $count;
      }

      //select user to assign task under company project
      public function assignUserTask($company_id,$project_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `users` WHERE `company_id`=:company_id AND `project_id`=:project_id");
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
        $stmt->execute();
        while($user =$stmt->fetch(PDO::FETCH_OBJ)){
          echo '
          <option value="'.$user->user_id.'"> '.$user->fullname.'</option>
          ';
        }
      }
      //delete task of a project
      public function deleteProjectTask($task_id){
            $stmt =$this->pdo->prepare("DELETE FROM `task` WHERE `task_id`=:task_id");
            $stmt->bindParam(":task_id",$task_id,PDO::PARAM_STR);
            $stmt->execute();
            header('Location: project.php');
      }

      //company project task list
      public function projectTask($company_id,$project_id){
        $stmt =$this->pdo->prepare("SELECT * FROM `task` WHERE `company_id`=:company_id AND `project_id`=:project_id");
        $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
        $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
        $stmt->execute();
        while($task =$stmt->fetch(PDO::FETCH_OBJ)){
          echo '
          <tr class="">
            <td class="inbox-small-cells">
              <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
            <td class="view-message dont-show"><a>'.$task->user_name.'</a></td>
            <td class="view-message"><a>'.$task->to_do.'</a></td>
            <td class="view-message inbox-small-cells">'.$this->timeAgo($task->created_at).'</td>
            <td class="view-message text-right"><span class="btn btn-xs '.(($task->completed == 0)? 'btn-warning': 'btn-success').'">'.(($task->completed == 0)? 'pending': 'Done').'</span></td>
            <td class="view-message text-right">
            <a href="viewProject.php?delete='.$task->task_id.'"><span class="btn btn-danger btn-xs"><i class=" fa fa-times-circle"></i></span></a>
            </td>

          </tr>
          ';
        }
      }


      // display company projects on project.php
      public function companyProject($company_id){
        $pID =$company_id;
        $stmt =$this->pdo->prepare("SELECT * FROM project WHERE `company_id`= $pID");
        $stmt->bindValue(":company_id",$pID,PDO::PARAM_STR);
        $stmt->execute();
        while($p =$stmt->fetch(PDO::FETCH_OBJ)){
      echo '<div class="col-md-4 col-sm-4 mb">
        <div class="stock card">
          <div class="stock-chart">
            <div id="chart"></div>
          </div>
          <div class="stock current-price">
            <div class="row">
              <div class="info col-sm-9 col-xs-9"><abbr>'.$p->project_name.'</abbr>
                <time>'.$p->created_at.'</time>
              </div>
            </div>
          </div>
          <div class="stock current-price">
            <div class="row">
              <div class="info col-sm-6 col-xs-6">
              <a href="viewProject.php?view='.$p->project_id.'"><button class="btn btn-block btn-outline-dark">View Project</button></a>
              </div>
                <div class="info col-sm-6 col-xs-6">
                <a href="project.php?delete='.$p->project_id.'"><button class="btn btn-block btn-outline-light">Delete Project</button></a>
                </div>
            </div>
          </div>


          </div>
      </div>';
        }
      }

  //get company users or workers
     public function users(){
       $pID =$_SESSION['company_id'];
       $stmt =$this->pdo->prepare("SELECT * FROM users WHERE `company_id`= $pID");
       $stmt->bindValue(":company_id",$pID,PDO::PARAM_STR);
       $stmt->execute();
       while($users =$stmt->fetch(PDO::FETCH_OBJ)){
         echo '<li>
                <a">
                <img class="img-circle" src="img/friends/fr-01.jpg" width="32">
                '.$users->fullname.'
                <span class="text-muted"></span>
                </a>
              </li>';
       }
     }

      // display team members on members.php
     public function teamMembers(){
      $pID =$_SESSION['company_id'];
      $stmt =$this->pdo->prepare("SELECT * FROM users WHERE `company_id`= $pID");
      $stmt->bindValue(":company_id",$pID,PDO::PARAM_STR);
      $stmt->execute();
      while($users =$stmt->fetch(PDO::FETCH_OBJ)){
        echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="custom-box">
          <a href="members.php?deleteUser='.$users->user_id.'"><button type="button"  class="close" name="button" data-dismiss ="modal" aria-label="close"><span aria-hidden="true">&times;</span></button></a>
            <div class="servicetitle">
              <h4>'.$users->work.'</h4>
              <hr>
            </div>
            <div class="icn-main-container">
              <span class="icn-container">user</span>
            </div>

            <ul class="pricing">
              <li>Name:<strong>'.$users->fullname.'</strong></li>
              <li><i class="fa fa-email"></i>'.$users->email.'</li>
            </ul>
            <a class="btn btn-info" href="profile.php?user='.$users->user_id.'">View Profile</a>
          </div>
        </div>  ';
      }
     }

     //show members on rightbar.php
     public function rightbarMembersView(){
      $pID =$_SESSION['company_id'];
      $stmt =$this->pdo->prepare("SELECT * FROM users WHERE `company_id`= $pID LIMIT 2 ");
      $stmt->bindValue(":company_id",$pID,PDO::PARAM_STR);
      $stmt->execute();
      while($users =$stmt->fetch(PDO::FETCH_OBJ)){
      echo '<div class="desc">
        <div class="thumb">
          <img class="img-circle" src="img/ui-zac.jpg" width="35px" height="35px" align="">
        </div>
        <div class="details">
          <p>
            <a href="profile.php?user='.$users->user_id.'"><strong>'.$users->fullname.'</strong></a><br/>
            <muted>'.$users->email.'</muted>
          </p>
        </div>
      </div>';
    }
  }

public function rightbarRecentProject($company_id){
  $stmt =$this->pdo->prepare("SELECT * FROM project WHERE `company_id`=:company_id LIMIT 3 ");
  $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
  $stmt->execute();
  while($proj =$stmt->fetch(PDO::FETCH_OBJ)){
    echo '<a href="viewProject.php?view='.$proj->project_id.'"><div class="desc">
      <div class="thumb">
        <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
      </div>
      <div class="details">
        <p>
          <muted>'.$proj->project_name.'</muted>
          <br/>
        Created at <a href="#">'.$proj->created_at.'</a>.<br/>
        </p>
      </div>
    </div></a>';
  }
}
//fetch a project details
     public function projectDetails($project_id){
       $stmt =$this->pdo->prepare("SELECT * FROM project WHERE `project_id`=:project_id");
       $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
       $stmt->execute();
       return $stmt->fetch(PDO::FETCH_OBJ);
     }

//delete user under company
public function deleteUser($company_id, $user_id){
  $stmt =$this->pdo->prepare("DELETE FROM users WHERE `company_id`=:company_id AND `user_id`=:user_id");
  $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
  $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
  $stmt->execute();

}

//delete user task
public function removeUserTask($company_id, $user_id){
  $stmt =$this->pdo->prepare("DELETE FROM task WHERE `company_id`=:company_id AND `user_id`=:user_id");
  $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
  $stmt->bindParam(":user_id",$user_id,PDO::PARAM_STR);
  $stmt->execute();

}
public function getBarHeight($project_id,$company_id){
  $stmt =$this->pdo->prepare("SELECT * FROM task WHERE `company_id`=:company_id  AND `project_id`=:project_id");
  $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
  $stmt->bindParam(":project_id",$project_id,PDO::PARAM_STR);
  $stmt->execute();
  return $count =$stmt->rowCount()*10;

}

public function drawBarChart($company_id){
  $stmt =$this->pdo->prepare("SELECT * FROM project WHERE `company_id`=:company_id ");
  $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
  $stmt->execute();
  while($bar =$stmt->fetch(PDO::FETCH_OBJ)){
    echo '<div class="bar">
      <div class="title">'.$bar->project_name.'</div>
      <div class="value tooltips" data-original-title="'. $this->getBarHeight($bar->project_id,$bar->company_id).'" data-toggle="tooltip" data-placement="top">'. $this->getBarHeight($bar->project_id,$bar->company_id).'%</div>
    </div>';
  }
}
//get company completed project;
public function companyProjectCompleted($company_id){
    $stmt =$this->pdo->prepare("SELECT * FROM project WHERE `company_id`=:company_id AND `completed`=1");
    $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
    $stmt->execute();
    return $count =$stmt->rowCount();

}

//get company completed project;
public function companyProjectCount($company_id){
    $stmt =$this->pdo->prepare("SELECT * FROM project WHERE `company_id`=:company_id");
    $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
    $stmt->execute();
    return $count =$stmt->rowCount();

}

//get company workers count
public function getUsersCount($company_id){
    $stmt =$this->pdo->prepare("SELECT * FROM users WHERE `company_id`=:company_id ");
    $stmt->bindParam(":company_id",$company_id,PDO::PARAM_STR);
    $stmt->execute();
    return $count =$stmt->rowCount();

}

///a time ago function for task
public function timeAgo($datetime){
  $time = strtotime($datetime);
  $current = time();
  $seconds = $current - $time;
  $minutes = round($seconds / 60);
  $hours   = round($seconds / 3600);
  $months  = round($seconds/2600640);

  if($seconds  <= 60){
    if($seconds == 0){
      return 'now';
    }
  }elseif ($minutes <= 60) {
    return $minutes."m";
  }elseif ($hours <= 24) {
    return $hours."h";
  }elseif ($months <= 12) {
     return date('M j', $time);
  }else{
    return date('j M Y', $time);
  }
}



}?>
